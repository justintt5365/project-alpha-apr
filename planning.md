## Feature 1 - Install dependencies
* [x] Fork and clone the starter project from Project Alpha
* [x] Create a new virtual environment in the repository directory for the project
* [x] Activate the virtual environment
* [x] Upgrade pip
* [x] Install django
* [x] Install black
* [x] Install flake8
* [x] Install djhtml
* [x] Deactivate your virtual environment
* [x] Activate your virtual environment
* [x] Use pip freeze to generate a requirements.txt file
* [x] test - python -m unittest tests.test_feature_01
* [x] add, commit, push
---
## Feature 2 - Set up the Django project and apps
* [x] Create a Django project named tracker so that the manage.py file is in the top directory
* [x] Create a Django app named accounts and install it in the tracker Django project in the INSTALLED_APPS list
* [x] Create a Django app named projects and install it in the tracker Django project in the INSTALLED_APPS list
* [x] Create a Django app named tasks and install it in the tracker Django project in the INSTALLED_APPS list
* [x] Run the migrations
* [x] Create a super user
* [x] test
* [x] add, commit, push
---
## Feature 3 - The Project model
* [x] Project model should have the following attributes:
  * [x] name 	string 	maximum length of 200 characters
  * [x] description 	string 	no maximum length
  * [x] members 	many-to-many 	Refers to the auth.User model, related name "projects"
* [x] Project model should implicitly convert to a string with the __str__ method that is the value of the name property.
* [x] Make migrations and migrate your database.
* [x] test
* [x] add, commit, push
---
## Feature 4 - Registering Project in the admin
* [x] Register the Project model with the admin so that you can see it in the Django admin site.
* [x] test
* [x] add, commit, push
---
## Feature 5 - The Project list view
* [x] Create a view that will get all of the instances of the Project model and puts them in the context for the template
* [x] Register that view in the projects app for the path "" and the name "list_projects" in a new file named projects/urls.py
* [x] Include the URL patterns from the projects app in the tracker project with the prefix "projects/"
* [x] Create a template for the list view that complies with the learn specifications
* [x] test
* [x] add, commit, push
---
## Feature 6 - Default path redirect
* [x] In the tracker urls.py, use the RedirectView to redirect from "" to the name of path for the list view that you created in the previous feature. Register that path a name of "home".
* [x] test
* [x] add, commit, push
---
## Feature 7 - Login page
* [x] Register the LoginView in your accounts urls.py with the path "login/" and the name "login"
* [x] Include the URL patterns from the accounts app in the tracker project with the prefix "accounts/"
* [x] Create a templates directory under accounts
* [x] Create a registration directory under templates
* [x] Create an HTML template named login.html in the registration directory
* [x] Put a post form in the login.html and any other required HTML or template inheritance stuff that you need to make it a valid Web page with the fundamental five (see specifications below)
* [x] In the tracker settings.py file, create and set the variable LOGIN_REDIRECT_URL to the value "home" which will redirect us to the path (not yet created) with the name "home"
* [x] test
* [x] add, commit, push
---
## Feature 8 - Require login for Project list view
* [x] Protect the list view for the Project model so that only a person that has logged in can access it
* [x] Change the queryset of the view to filter the Project objects where members equals the logged in user
* [x] test
* [x] add, commit, push
---
## Feature 9 - Logout page
* [x] Import the LogoutView from the same module that you imported the LoginView from
* [x] Register that view in your urlpatterns list with the path "logout/" and the name "logout"
* [x] In the tracker settings.py file, create and set the variable LOGOUT_REDIRECT_URL to the value "login" which will redirect the logout view to the login page
* [x] test
* [x] add, commit, push
---
## Feature 10 - Sign up page
* [x] create it in the file in the accounts directory that should hold the views.
* [x] You'll need to import the UserCreationForm from the built-in auth forms 
* [x] You'll need to use the special create_user method to create a new user account from their username and password
* [x] You'll need to use the login function that logs an account in
* [x] After you have created the user, redirect the browser to the path registered with the name "home"
* [x] Create an HTML template named signup.html in the registration directory
* [x] Put a post form in the signup.html and any other required HTML or template inheritance stuff that you need to make it a valid Web page with the fundamental five (see specifications below)
* [x] test
* [x] add, commit, push
---
## Feature 11 - The Task model
* [x] create a Task model in the tasks Django app.
* [x] name 	string 	maximum length of 200 characters
* [x] start_date 	date-time
* [x] due_date 	date-time
* [x] is_completed 	Boolean 	default value should be False
* [x] project 	foreign key 	Refers to the projects.Project model, related name "tasks", on delete cascade
* [x] assignee 	foreign key 	Refers to the auth.User model, null is True, related name "tasks", on delete set null
* [x] the Task model should implicitly convert to a string with the __str__ method that is the value of the name property.
* [x] Make migrations and migrate your database.
* [x] test
* [x] add, commit, push
---
## Feature 12 - Registering Task in the admin
* [x] Register the Task model with the admin so that you can see it in the Django admin site.
* [x] test
* [x] add, commit, push
---
## Feature 13 - The Project detail view
* [x] Create a view that shows the details of a particular project
* [x] A user must be logged in to see the view
* [x] In the projects urls.py file, register the view with the path "<int:pk>/" and the name "show_project"
* [x] Create a template to show the details of the project and a table of its tasks
* [x] Update the list template to show the number of tasks for a project
* [x] Update the list template to have a link from the project name to the detail view for that project
* [x] test
* [x] add, commit, push
---
## Feature 14 - The Project create view
* [x] Create a create view for the Project model that will show the name, description, and members properties in the form and handle the form submission to create a new Project
* [x] A person must be logged in to see the view
* [x] If the project is successfully created, it should redirect to the detail page for that project
* [x] Register that view for the path "create/" in the projects urls.py and the name "create_project"
* [x] Create an HTML template that shows the form to create a new Project (see the template specifications below)
* [x] Add a link to the list view for the Project that navigates to the new create view
* [x] test
* [x] add, commit, push
---
## Feature 15 - The Task create view
* [x] Create a view that will show a form to create an instance of the Task model for all properties except the is_completed field 
* [x] The view must only be accessible by people that are logged in
* [x] When the view successfully handles the form submission, have it redirect to the detail page of the task's project
* [x] Register that view in the tasks app for the path "create/" and the name "create_task" in a new file named tasks/urls.py
* [x] Include the URL patterns from the tasks app in the tracker project with the prefix "tasks/"
* [x] Create a template for the create view that complies with the following specifications
* [x] Add a link to create a task from the project detail page that complies with the following specifications
* [x] test 
* [x] add, commit, push
---
## Feature 16 - Show "My Tasks" list view
* [x] Create a list view for the Task model with the objects filtered so that the person only sees the tasks assigned to them by filtering with the assignee equal to the currently logged in user
* [x] The view must only be accessible by people that are logged in
* [x] Register that view in the tasks app for the path "mine/" and the name "show_my_tasks" in the tasks urls.py file
* [x] Create an HTML template that conforms with the following specification
* [x] test
* [x] add, commit, push
---
## Feature 17 - Completing a task
* [x] Create an update view for the Task model that only is concerned with the is_completed field
* [x] When the view successfully handles a submission, it should redirect to the "show_my_tasks" URL path, that is, it should redirect to the "My Tasks" view (success_url property on a view class)
* [x] Register that view in the tasks app for the path "<int:pk>/complete/" and the name "complete_task" in the tasks urls.py file
* [x] You do not need to make a template for this view
* [x] Modify the "My Tasks" view to comply with the template specification
* [x] test
* [x] add, commit, push
---
## Feature 18 - Markdownify
* [x] Install the django-markdownify package using pip (the first pip command in the instructions) and put it into the INSTALLED_APPS in the tracker settings.py according to the Installation instructions block
* [x] Also, in the tracker settings.py file, add the configuration setting to disable sanitation
* [x] In your the template for the Project detail view, load the markdownify template library as shown in the Usage 
* [x] Replace the p tag and {{ project.description }} in the Project detail view with this code
{{ project.description|markdownify }}
* [x] Use pip freeze to update your requirements.txt file
* [x] test
* [x] add, commit, push
---
## Feature 19 - Navigation
* [x] On all HTML pages, add
  * [x] a header tag as the first child of the body tag before the main tag that contains 
    * [x] a nav tag that contains 
      * [x] a ul tag that contains 
        * [x] if the person is logged in, 
          * [x] an li tag that contains 
            * [x] an a tag with an href to the "show_my_tasks" path with the content "My tasks"
          * [x] an li tag that contains
            * [x] an a tag with an href to the "list_projects" path with the content "My projects 
          * [x] an li tag that contains 
            * [x] an a tag with an href to the "logout" path with the content "Logout"
          * [x] otherwise
            * [x] an li tag that contains 
              * [x] an a tag with an href to the "login" path with the content "Login"
            * [x] an li tag that contains 
              * [x] an a tag with an href to the "signup" path with the content "Signup"
* [x] test
* [x] add, commit, push
---
## Check code quality
* [x] flake8 accounts projects tasks
* [x] black --check accounts projects tasks
* [x] djhtml -i «path to HTML template»