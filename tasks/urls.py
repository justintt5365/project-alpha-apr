from django.urls import path

from tasks.views import TaskCreateView, TaskView, TaskUpdate

urlpatterns = [
    path("create/", TaskCreateView.as_view(), name="create_task"),
    path("mine/", TaskView.as_view(), name="show_my_tasks"),
    path("<int:pk>/complete/", TaskUpdate.as_view(), name="complete_task"),
]
